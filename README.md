# ABP Tabulation and Indentation

This small plugin allows to use [indent] and [tab] MyCode.

# Usage
## Indent
```
[indent]some contents
and also
[indent]another indented content[/indent]
[/indent]
```
gives:
```
    some contents
    and also
        another indented content
```
# Tab
```
[tab]A simple indented line
and the following
[tab=3]Third level indented line
[tab=2]and the following
```
gives:
```
    A simple indented line
and the following
            Third level indented line
        and the following
```