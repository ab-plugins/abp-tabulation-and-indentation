<?php
/************************************************************
* author       : CrazyCat
* plugin       : Indent MyCode
* version      : 1.0
*************************************************************/
if(!defined('IN_MYBB'))
{
	die("This file cannot be accessed");
}

$plugins->add_hook("parse_message", "indent_run");

function indent_info () {
	return array (
		"name"			=> "Tag [indent]",
		"description"	=> "Adds an indent tag",
		'website'		=> 'http://www.g33k-zone.org',
		"author"		=> "CrazyCat",
		"version"		=> "1.0",
		"compatibility"	=> "16*,18*",
		"guid"			=> "48be17fc4dcaa9cc555bca455c811cea"
	);
}

function indent_activate () {
}

function indent_deactivate () {
}

function indent_run ($message) {
	$message = str_ireplace(array('[indent]', '[/indent]'), array('<div style="margin-left: 1em;">', '</div>'), $message);
	$message = preg_replace('!\[tab\]!si', '<span style="display: inline-block; width: 1em;"></span>', $message);
	$message = preg_replace('!\[tab=([1-9]{1})\]!si', '<span style="display: inline-block; width: $1em;"></span>', $message);
	return $message;
}

?>
